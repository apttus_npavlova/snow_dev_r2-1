﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Model
{
    public class QuoteReleaseVersionModel
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the instance.
        /// </summary>
        /// <value>
        /// The type of the instance.
        /// </value>
        [JsonProperty("ext_InstanceType")]
        public string InstanceType { get; set; }

        /// <summary>
        /// Gets or sets the release.
        /// </summary>
        /// <value>
        /// The release.
        /// </value>
        [JsonProperty("ext_Release")]
        public string Release { get; set; }

        /// <summary>
        /// Gets or sets the release version.
        /// </summary>
        /// <value>
        /// The release version.
        /// </value>
        [JsonProperty("ext_ReleaseVersion")]
        public string ReleaseVersion { get; set; }
    }
}
