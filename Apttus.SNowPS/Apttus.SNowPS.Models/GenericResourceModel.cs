﻿/*************************************************************
@Name: GenericResourceModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 03-Nov-2017
@Description: This class contains classes & properties for 'Generic Resource' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

using Newtonsoft.Json;


namespace Apttus.SNowPS.Model
{
    public class GenericResourceModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string MasterAgreementId { get; set; }

        [JsonProperty(PropertyName = "crm_Product")]
        public LookUp Product { get; set; }

        [JsonProperty(PropertyName = "cpq_LineItem")]
        public LineItemModel LineItem { get; set; }

        [JsonProperty(PropertyName = "cpq_ProductClassification")]
        public ProductClassificationModel ProductClassification { get; set; }


        public class OptionId
        {
            public string Id { get; set; }
            public string Name { get; set; }


            [JsonProperty(PropertyName = "ext_RoleGeneric")]
            public KeyValue RoleGeneric { get; set; }

        }
        public class LineItemModel
        {
            public decimal? BasePrice { get; set; }
            public decimal? Quantity { get; set; }
            public decimal? NetPrice { get; set; }
            public OptionId OptionId { get; set; }
        }
        public class ProductClassificationModel
        {
            public ClassificationId ClassificationId { get; set; }
        }

        public class ClassificationId
        {
            public string Name { get; set; }
        }
    }
}
