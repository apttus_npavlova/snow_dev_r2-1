﻿using Apttus.SNowPS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    public class BaseController : ApiController
    {
        public string AccessToken { get; set; }

        public BaseController()
        {
            // Setting the property with access token
            AccessToken = Utilities.GetAuthToken();
        }
    }
}
