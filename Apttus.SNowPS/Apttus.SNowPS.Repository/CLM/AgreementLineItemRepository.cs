﻿/****************************************************************************************
@Name: AgreementLineItem.cs
@Author: Bhavinkumar Mistry
@CreateDate: 11 Nov 2017
@Description: Agreement Line Item related business logic
@UsedBy: This will be used by QuoteController.cs, AgreementController.cs

*****************************************************************************************/


using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Apttus.SNowPS.Repository.CLM
{
    /// <summary>
    /// Agreement Clause Repository
    /// </summary>
    public sealed class AgreementLineItemRepository
    {
        #region Private Fields
        /// <summary>
        /// Static object of AgreementLineItemRepository
        /// </summary>
        private static AgreementLineItemRepository agreementLineItemRepository = new AgreementLineItemRepository();
        #endregion

        #region Public Properties
        /// <summary>
        /// Property to store Authentication token
        /// </summary>
        public string AuthToken { get; set; }
        #endregion

        #region Constructors
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static AgreementLineItemRepository()
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Use to Get Single Instance of AgreementClauseRepository
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public static AgreementLineItemRepository Instance(string authToken)
        {
            if(agreementLineItemRepository != null)
            {
                agreementLineItemRepository.AuthToken = authToken;
            }
            return agreementLineItemRepository;
        }
        
        public string UpdateAgreement(AgreementLineItemModel agreementLineItemModel)
        {
            return Constants.MSG_SUCCESS;
        }

        /// <summary>
        /// Converts to HTML table row.
        /// </summary>
        /// <param name="agreementLineItemId">The agreement line item identifier.</param>
        /// <returns></returns>
        public string ConvertToHtmlTableRow(Guid agreementLineItemId)
        {
            var query = new Query
            {
                EntityName = AgreementLineItemConstants.ENTITYNAME,
                Columns = AgreementLineItemConstants.FIELDS_HTMLROW.ToList(),
                Criteria = new Expression
                {
                    Conditions = new List<Condition>
                    {
                        new Condition
                        {
                            FieldName = Constants.FIELD_ID,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                            Value = agreementLineItemId
                        }
                    }
                },
                Joins = new List<Join>
                {
                    new Join
                    {
                        EntityAlias= AgreementObjectConstants.OBJ_ALIAS,
                        FromEntity = AgreementLineItemConstants.ENTITYNAME,
                        FromAttribute = AgreementLineItemConstants.AGREEMENTID,
                        ToEntity = AgreementObjectConstants.ENTITYNAME,
                        ToAttribute = Constants.FIELD_ID
                    }
                }
            };

            var response = Utilities.Search(query.Serialize(), new RequestConfigModel
            {
                accessToken = AuthToken,
                searchType = SearchType.AQL,
                objectName = AgreementLineItemConstants.ENTITYNAME
            });

            if (response?.Content != null)
            {
                var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                var agreementLineItems = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                if(agreementLineItems!=null && agreementLineItems.Count > 0)
                {
                    var agreementLineItem = agreementLineItems[0];
                    if (agreementLineItem != null)
                    {
                        var agreement = agreementLineItem.ContainsKey(AgreementObjectConstants.OBJ_ALIAS) ? JsonConvert.DeserializeObject<Dictionary<string, object>>(agreementLineItem[AgreementObjectConstants.OBJ_ALIAS].ToString()) : null;
                        if (agreement != null)
                        {
                            string productName = agreementLineItem.ContainsKey(AgreementLineItemConstants.PRODUCTID) ? JsonConvert.DeserializeObject<LookUp>(agreementLineItem[AgreementLineItemConstants.PRODUCTID].ToString()).Name : string.Empty;

                            decimal quantity = agreementLineItem.ContainsKey(AgreementLineItemConstants.QUANTITY) ? agreementLineItem[AgreementLineItemConstants.QUANTITY].ConvertToDecimal() : decimal.Zero;

                            decimal salesPrice = agreementLineItem.ContainsKey(AgreementLineItemConstants.EXT_SALESPRICE) ? agreementLineItem[AgreementLineItemConstants.EXT_SALESPRICE].ConvertToDecimal() : decimal.Zero;

                            decimal acv = agreementLineItem.ContainsKey(AgreementLineItemConstants.EXT_ANNUALCONTRACTVALUE) ? agreementLineItem[AgreementLineItemConstants.EXT_ANNUALCONTRACTVALUE].ConvertToDecimal() : decimal.Zero;

                            decimal nnacv = agreementLineItem.ContainsKey(AgreementLineItemConstants.EXT_NETNEWACV) ? agreementLineItem[AgreementLineItemConstants.EXT_NETNEWACV].ConvertToDecimal() : decimal.Zero;

                            decimal totalValue = agreementLineItem.ContainsKey(AgreementLineItemConstants.EXT_USDTOTALVALUE) ? agreementLineItem[AgreementLineItemConstants.EXT_USDTOTALVALUE].ConvertToDecimal() : decimal.Zero;

                            string agreementLineItemData = agreement.ContainsKey(AgreementObjectConstants.EXT_AGREEMENTLINEITEMDATA) ? agreement[AgreementObjectConstants.EXT_AGREEMENTLINEITEMDATA].ConvertToString() : string.Empty;

                            string htmlRow = agreementLineItemData + string.Format(AgreementLineItemConstants.HTML_ROW_FORMAT, productName, quantity, salesPrice, "", acv, nnacv, totalValue);

                            var agreements = new List<dynamic>();

                            
                            agreements.Add(new
                            {
                                Id = agreement[Constants.FIELD_ID],
                                ext_AgreementLineItemData = htmlRow
                            });
                            var updateResponse = Utilities.Update(agreements, new RequestConfigModel
                            {
                                objectName = AgreementObjectConstants.ENTITYNAME,
                                resolvedID = agreement[Constants.FIELD_ID].ToString(),
                                accessToken = AuthToken
                            });

                            return updateResponse?.Content?.ReadAsStringAsync().Result;
                        }
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Populates the instance details.
        /// </summary>
        /// <param name="agreementLineItemId">The agreement line item identifier.</param>
        /// <returns></returns>
        public string PopulateInstanceDetails(Guid agreementLineItemId)
        {
            var query = new Query
            {
                EntityName = AgreementLineItemConstants.ENTITYNAME,
                Columns = AgreementLineItemConstants.FIELDS_HTMLROW.ToList(),
                Criteria = new Expression
                {
                    Conditions = new List<Condition>
                    {
                        new Condition
                        {
                            FieldName = Constants.FIELD_ID,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                            Value = agreementLineItemId
                        }
                    }
                },
                Joins = new List<Join>
                {
                    new Join
                    {
                        EntityAlias= AgreementObjectConstants.OBJ_ALIAS,
                        FromEntity = AgreementLineItemConstants.ENTITYNAME,
                        FromAttribute = AgreementLineItemConstants.AGREEMENTID,
                        ToEntity = AgreementObjectConstants.ENTITYNAME,
                        ToAttribute = Constants.FIELD_ID
                    }
                }
            };

            var response = Utilities.Search(query.Serialize(), new RequestConfigModel
            {
                accessToken = AuthToken,
                searchType = SearchType.AQL,
                objectName = AgreementLineItemConstants.ENTITYNAME
            });

            if (response?.Content != null)
            {
                var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                var agreementLineItems = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                if (agreementLineItems != null && agreementLineItems.Count > 0)
                {
                    var agreementLineItem = agreementLineItems[0];
                    var instanceDetails = new List<dynamic>();
                    instanceDetails.Add(new
                    {
                        ext_AgreementLineItemId = agreementLineItem[Constants.FIELD_ID],
                        ext_DataCenter="",
                        ext_InstanceName= agreementLineItem[AgreementLineItemConstants.EXT_INSTANCENAME],
                        ext_InstanceType="",
                        ext_StorageLimit="",
                    });
                    Utilities.Create(instanceDetails, new RequestConfigModel
                    {
                        accessToken = AuthToken,
                        objectName = InstanceDetailsConstants.ENTITYNAME
                    });
                }
            }

            return string.Empty;
        }
        #endregion
    }
}
